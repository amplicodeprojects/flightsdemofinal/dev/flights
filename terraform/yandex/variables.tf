variable "name" {
    type    = string
    default = "yandex"
}

variable "image" {
    type    = string
}

variable "port" {
    type    = number
    default = 8081
}

variable "instance_cores" {
    type    = number
    default = 2
}

variable "instance_memory" {
    type    = number
    default = 2
}

variable "platform" {
    type = string
    default = "standard-v1"
}

locals {
    env = [
        {
            name  = "LEGACYFLIGHTS_DB_NAME"
            value = module.legacyflights.name
        },
        {
            name  = "LEGACYFLIGHTS_DB_HOST"
            value = module.legacyflights.host
        },
        {
            name  = "LEGACYFLIGHTS_DB_PORT"
            value = module.legacyflights.port
        },
        {
            name  = "LEGACYFLIGHTS_DB_USER"
            value = module.legacyflights.username
        },
        {
            name  = "LEGACYFLIGHTS_DB_PASSWORD"
            value = module.legacyflights.password
        },
      {
        name  = "SPRING_PROFILES_ACTIVE",
        value = "yandex"
      }
    ]
}

variable "folder_id" {
    type    = string
    default = ""
}

variable "zone" {
    type    = string
    default = "ru-central1-a"
}

variable "docker_logs" {
  type    = bool
  default = true
}

variable "extended_metrics" {
  type    = bool
  default = false
}

variable "alb_logs" {
  type    = bool
  default = true
}

variable "retention_period" {
  type    = number
  default = 1
}

variable "cidr_block" {
    type    = string
    default = "10.0.0.0/16"
}
