output "ssh_key" {
  value = tls_private_key.yandex-keys.private_key_pem
}

output "target_group_id" {
  value = yandex_compute_instance_group.instance_group.application_load_balancer.0.target_group_id
}

output "instance_sg_id" {
  value = yandex_vpc_security_group.sg-instance.id
}
