module "legacyflights" {
    source = "./postgres"

    db_instance                  = "b1.medium"
    engine_version               = "15"
    name                         = "legacyflights"
    random_password              = true
    password                     = "root"
    zone                         = var.zone
    storage                      = 10
    subnet_id                    = module.vpc.subnet_id
    username                     = "root"
    vpc_id                       = module.vpc.vpc_id

}
